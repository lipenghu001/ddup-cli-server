'use strict';

const Controller = require('egg').Controller;
// const mongo = require('../utils/mongo');

const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/';

class ProjectController extends Controller {
  // 1. 获取项目/组件的代码模板
  async getTemplate() {
    const { ctx } = this;
    let conn = null;
    try {
      conn = await MongoClient.connect(url);
      console.log('数据库已连接');
      const test = conn.db('ddup-cli').collection('project');
      // 查询
      const arr = await test.find().toArray();
      ctx.body = {
        code: 0,
        message: '',
        data: arr,
      };
    } catch (e) {
      console.log('错误', e);
    } finally {
      if (conn !== null) {
        conn.close();
        console.log('执行到finally, 数据库连接已关闭');
      }
    }
    // const data = await mongo().query('project');
    // console.log('data', data);
    // ctx.body = data;
  }
}

module.exports = ProjectController;
