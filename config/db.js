'use strict';

/** MONGODB **/
const mongodbUrl = 'mongodb://hulipeng:123456@localhost:27017/ddup-cli';
const mongodbName = 'ddup-cli';

module.exports = {
  mongodbUrl,
  mongodbName,
};
